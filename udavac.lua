#!/usr/bin/lua
-- udavacz v1.0
--
-- de-anonimize users behind nat.
-- returns first 9 chars of dns, i.e. your dns
-- better be accurate.
--
-- (c) 2008 kt@leet.cz
--

require "socket"
serv=socket.tcp()
serv:settimeout(0)
serv:setoption("reuseaddr",true)
assert(serv:bind("0.0.0.0",113))
serv:listen(100)

ins={serv}

while true do
	local r = socket.select(ins, nil)
	if r[serv] then
		local newc=serv:accept()
		table.insert(ins,newc)
	else
		for _,v in ipairs(r) do
			local ln = v:receive("*l") or ""
			local p1, p2 = ln:match("^(%d*)[ \t]*,[ \t]*(%d*)")
			if ln and p1 and p2 then socket.protect(function()
				p1 = tonumber(p1)
				p2 = tonumber(p2)
				local remhost, remport = socket.try(v:getpeername())
				local rp=io.popen("conntrack -L conntrack -p tcp --state ESTABLISHED --orig-port-dst "..p2.." --reply-port-dst "..p1.." -d "..remhost,"r")
				local rpl = rp:read("*l") or ""
				rp:close()
				local h=rpl:match(".-src%=([^ ]*) ")
				if h then
					local hn=socket.try(socket.dns.tohostname(h or ""))
					local rhn=hn:match("^([^.]*)"):sub(1,9)
					socket.try(v:setoption("linger", {on=true,timeout=5}))
					socket.try(v:send(p1.." , "..p2.." : USERID : UNIX : "..rhn.."\r\n"))
				end
			end)() end

			for idx,tv in ipairs(ins) do
				if tv==v then
					v:close()
					table.remove(ins,idx)
				end
			end
		end
	end
end
